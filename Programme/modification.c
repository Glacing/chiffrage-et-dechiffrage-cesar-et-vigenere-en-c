#include <stdio.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <stdbool.h>

//procedure suppression d'accents et majuscules
void supprimerAccents (wchar_t messa[]) {
	//tableau de caractères accentués
	wchar_t Accents[50] = {L"àÀâÂäÄçÇéÉèÈêÊëËïÏîÎôÔöÖùÙûÛüÜÿŸ"};
	//tableau de caracètres non accentués correspondant
	wchar_t NonAccents[50] = {L"aaaaaacceeeeeeeeiiiioooouuuuuuyy"};
	//boucle pour enlever accents et majuscules
	for(int i = 0;i < wcslen(messa); i++) {
		for(int j = 0; j < wcslen(Accents);j++) {
			if (messa[i] == Accents[j]) {
				messa[i] = NonAccents[j];
			}
			messa[i] = tolower(messa[i]);
		}
	}
}

//fonction qui renvoie la presence d'un caractère invalide ou non
int caractereInvalide(wchar_t messa[]) {
	//tableau de caractères invalides
	wchar_t CaracInvalides[50] = {L"0123456789/*-+,;.:/!\{}()[]_^$£>&~#'`<>%?!"};
	//integer de retour
	int erreur = 0;
	//boucle de verification de la presence d'un caractere invalide
	for (int i = 0;i < wcslen(messa);i++) {
		for(int j = 0;j < wcslen(CaracInvalides);j++) {
			if (messa[i] == CaracInvalides[j]) {
				erreur = 1;
			}
		}
	}
	return erreur;
}

//procedure qui permet de retourner le message sans caractères d'entrée
void remplirMessage (wchar_t messa[]) {
	wchar_t charac;
	int taillemess = wcslen(messa);
	for (int i=0; i<taillemess;i++) {
		charac = messa[i];
		if (charac == '\n') {
			messa[wcslen(messa)-1] = '\0';
		}
	}
}
