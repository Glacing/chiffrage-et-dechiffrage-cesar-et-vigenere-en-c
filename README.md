# **M2101 - Mini-Projet**

**Mini projet de chiffrement/déchiffrement en C**

lien Sylvain :
[https://framagit.org/Slyveone/m2101-mini-projet/](https://framagit.org/Slyveone/m2101-mini-projet/)

lien Aurélien :
[https://framagit.org/Glacing/chiffrage-et-dechiffrage-cesar-et-vigenere-en-c/](https://framagit.org/Glacing/chiffrage-et-dechiffrage-cesar-et-vigenere-en-c/)

par FERRE Aurélien  et  ROEHRIG Sylvain

Sommaire

* [*Objectif*](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#objectif)
* [*Fonctions*](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#fonctions)
* [*Cas d'erreurs*](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#cas-derreurs)
* [*Informations annexes*](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#informations-annexes)



# *Objectif*


L'objectif de l'application est de permettre à l'utilisateur
de chiffrer ou déchiffrer un message à l'aide des méthodes
de César et de Vigenère. 

Le programme devra aussi être capable
de convertir les lettres accentuées en lettres sans accents, 
pour faciliter le chiffrement, et affichera un message d'erreur
si des caractères spéciaux ; c-à-d des caractères qui ne sont
pas alphanumériques (A-Z et 0-9, à l'exception des accents) ; 
sont utilisés.

Additionellement, si le temps nous est suffisant, le programme 
pourrait aussi sauvegarder dans un fichier les différents 
résultats des chiffrements et déchiffrements.



# *Fonctions*

* **`void main();`**

Cette fonction est le programme. Elle lance les fonctions
suivantes dans un ordre assurant le bon fonctionnement de
l'application. Elle ne prend rien en entrée, et ne retourne rien.

* **`void remplirMessage(wchar_t messa[]);`**

Cette fonction prend en entrée une liste de caractères
qui sera remplie par l'utilisateur, puis retournée une fois vidée des caractères d'"entrée".

* **`void supprimerAccents(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et retourne cette même liste avec ses caractères en minuscule et sans accents.

* **`int caractereInvalide(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie.

Si cette liste contient des caractères spéciaux (qui ne sont pas
alphanumériques), alors cette fonction retourne 1, et le programme s'arrête
et renvoie un
[message d'erreur](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#cas-derreurs).
Sinon, le programme continue normalement.

* **`void choixChiffrage(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et propose à l'utilisateur de choisir s'il souhaite chiffrer ou
déchiffrer le message, avec la méthode qu'il souhaite.

* **`void chiffrageCesar(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et lance la fonction cesar avec pour paramètre le chiffrement
(c-à-d sous la forme "cesar(true,messa);")".

* **`void dechiffrageCesar(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et lance la fonction cesar avec pour paramètre le déchiffrement
(c-à-d sous la forme "cesar(false,messa);")".

* **`void cesar(bool chiffrage, wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie et un booléen.
Il retourne le contenu de cette liste suite suite à son chiffrement si le booléen
en entrée équivaut à VRAI, ou à son déchiffrement si le booléen équivaut à FAUX,
en suivant la méthode de César, à l'aide d'une clé choisie par l'utilisateur.

* **`void chiffrageVigenere(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et lance la fonction vigenere avec pour paramètre le chiffrement
(c-à-d sous la forme "vigenere(true,messa);")".

* **`void dechiffrageVigenere(wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie
et lance la fonction vigenere avec pour paramètre le déchiffrement
(c-à-d sous la forme "vigenere(false,messa);")".

* **`void vigenere(bool chiffrage, wchar_t messa[]);`**

Cette fonction prend en entrée la liste de caractères remplie et un booléen.
Il retourne le contenu de cette liste suite suite à son chiffrement si le booléen
en entrée équivaut à VRAI, ou à son déchiffrement si le booléen équivaut à FAUX,
en suivant la méthode de Vigenère, à l'aide d'une clé choisie par l'utilisateur.

* **`void ecritureFichier(wchar_t messa[]);`**

Cette fonction prend en entrée le message chiffré ou déchiffré par la fonction
cesar ou la fonction vigenere, et écrit celui-ci dans un fichier nommé "résultat"
(créé dans le même répertoire que le programme puis remplit s'il n'existe pas,
vidé puis remplit s'il existe).

# *Cas d'erreurs*

* **Erreurs levées contrôlées**

Si le message entré par l'utilisateur contient des caractères
spéciaux, la fonction caractereInvalide entraîne l'arrêt du programme
et affiche le message d'erreur suivant :

> "Présence de caractère invalide"

* **Autres erreurs levées**

Pour l'instant, aucune erreur possible n'a été relevée. Il est cependant possible
que le programme puisse ne pas fonctionner sous certaines conditions qui ne
furent pas prévues ou étudiées par les élèves ayant réalisé ce programme.



# *Informations annexes*


*IUT Informatique Toulouse III - Paul Sabatier*

*Année 2019-2020*

[https://framagit.org/Slyveone/m2101-mini-projet/](https://framagit.org/Slyveone/m2101-mini-projet/)

---

[Revenir en haut de la page](https://framagit.org/Slyveone/m2101-mini-projet/-/blob/master/README.md#m2101-mini-projet)

